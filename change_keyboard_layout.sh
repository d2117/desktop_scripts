#!/bin/bash -vex

# set keyboard layouts
setxkbmap -layout us,ru,ua 

pushd $PWD &>/dev/null
  cd $(dirname $0)/xkb-switch/ 
  language="$(./xkb-switch)"
  case "$language" in 
    "us") next_language="ru";;
    "ru") next_language="ua";;
    "ua") next_language="us";;
  esac
  ./xkb-switch -s "$next_language"
popd &>/dev/null

