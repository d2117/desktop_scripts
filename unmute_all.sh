#! /bin/bash

amixer set Master unmute
amixer set Front  unmute
amixer set Surround  unmute
amixer set Center  unmute
amixer set LFE  unmute
amixer set Side unmute
